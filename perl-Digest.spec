Name:           perl-Digest
Version:        1.20
Release:        4
Summary:        Modules that calculate message digests
License:        GPL-1.0-or-later OR Artistic-1.0-Perl
URL:            https://metacpan.org/release/Digest
Source0:        https://cpan.metacpan.org/authors/id/T/TO/TODDR/Digest-%{version}.tar.gz

BuildArch:      noarch
BuildRequires:  coreutils findutils make perl-interpreter perl-generators perl(Carp) perl(Exporter) perl(ExtUtils::MakeMaker) perl(MIME::Base64)
BuildRequires:  perl(lib) perl(Test::More)
Requires:       perl(MIME::Base64)

%description
The Digest:: modules calculate digests, also called "fingerprints" or
"hashes", of some data, called a message. The digest is (usually)
some small/fixed size string. The actual size of the digest depend of
the algorithm used. The message is simply a sequence of arbitrary
bytes or bits.

%package_help

%prep
%autosetup -n Digest-%{version} -p1
chmod -x digest-bench

%build
%{__perl} Makefile.PL NO_PACKLIST=1 INSTALLDIRS=vendor
%{make_build}

%install
make pure_install PERL_INSTALL_ROOT=%{buildroot}
%{_fixperms} %{buildroot}/*

%check
make test

%files
%doc digest-bench README Changes
%{perl_vendorlib}/*

%files help
%{_mandir}/*/*

%changelog
* Tue Jan 07 2025 Funda Wang <fundawang@yeah.net> - 1.20-4
- cleanup spec

* Sat Nov 26 2022 huyubiao <huyubiao@huawei.com> - 1.20-3
- Update the Source0 URL.

* Fri Jun 24 2022 Chenyx <chenyixiong3@huawei.com> - 1.20-2
- License compliance rectification

* Sat Dec 25 2021 tianwei <tianwei12@huawei.com> - 1.20-1
- Upgrade to 1.20

* Sun Sep 29 2019 yefei <yefei25@huawei.com> - 1.17-419
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:delete irrelevant information

* Sat Sep 14 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.17-418
- Package init
